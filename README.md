# README #

This repo contains a Visual Studio/C# console program that authenticates against the NTMCalc REST API and performs a call.

### What is this repository for? ###

Copy or use the code as an example on how to authenticate and call NTMCalc Rest API. You need to have an NTM user account and 
valid client credentials to make calls.

### How do I get set up? ###

Download and open solution in Visual Studio.

### Who do I talk to? ###

info@ntmcalc.org