﻿using System;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;


namespace ntmcalc_sample_csharp_client
{
    public static class Auth
    {

        private static dynamic authResponse;
        private static Expiration accessTokenExpiration;
        private static Expiration refreshTokenExpiration;
       
        private static dynamic credentials = new 
            {
                // put your own credentials in here
                UserName = "myUser",
                Password = "myPassword",
                ClientSecret = "XXXX-XXXXX-XXXXX-XXXXX",
                ClientId = "myClient"
            };
        private static dynamic authServer = new 
            {
                Domain = "auth.transportmeasures.org", // Authentication server domain
                TokenEndPointPath = "/auth/realms/ntm/protocol/openid-connect/token", // Path to the service issuing access tokens,
                LogoutEndPointPath = "/auth/realms/ntm/protocol/openid-connect/logout" // Path to the logout web service
            };

        private static string basicAuthorization =
                   Convert.ToBase64String(Encoding.ASCII.GetBytes(credentials.ClientId.ToString() + ":" + credentials.ClientSecret.ToString()));
       // }
        public static string GetAccessToken()
        {
            string parameters = "";
            // At the initial acquire of an access token or when the refresh token has expired: - prepare parameters to acquire an 
            // access token with user credentials (grant_type=password). Basic authorization (using Authorization header) with client id and client secret, is done.
            if (refreshTokenExpiration == null || refreshTokenExpiration.HasExpired())
            {
                parameters = string.Format("grant_type=password&username={0}&password={1}", credentials.UserName.ToString(),
                    credentials.Password.ToString());
                Console.WriteLine("Authentication against NTMCalc authentication server...");

            }
            // When access token has expired: - prepare parameters to acquire an access token with a refresh token (grant_type=refresh_token).
            else if (accessTokenExpiration == null || accessTokenExpiration.HasExpired())
            {
                parameters = string.Format("grant_type=refresh_token&refresh_token={0}", authResponse.refresh_token.ToString());
                // When the existing acces token is still valid: - just return it.
            }
            else
            {
                return authResponse.access_token;
            }
            var webAddr = "https://" + authServer.Domain.ToString() + authServer.TokenEndPointPath.ToString();

            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                client.Headers[HttpRequestHeader.Authorization] = "Basic " + basicAuthorization;
                authResponse =
                    JObject.Parse(client.UploadString(webAddr,
                        "POST", parameters));
            }
            //
            Console.WriteLine("Authentication succesful, new accesstoken was issued");
            accessTokenExpiration = new Expiration((int) authResponse.expires_in); // reset expiration time  
            refreshTokenExpiration = new Expiration((int) authResponse.refresh_expires_in);
            return authResponse.access_token.ToString();
        }

        public static string EndSession()
        {
            if (authResponse == null) return "Never logged in.";
            var webAddr = "https://" + authServer.Domain.ToString() + authServer.LogoutEndPointPath.ToString();
            string parameters = string.Format("refresh_token={0}", authResponse.refresh_token.ToString());
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                client.Headers[HttpRequestHeader.Authorization] = "Basic " + basicAuthorization;
                return "Session ended.";
            }
        }
    }
    public class Expiration
    {
        private readonly DateTime expirationTime;
        private int networkLatency = 5;

        public Expiration(int expiresInSeconds)
        {
            expirationTime = DateTime.Now.AddSeconds(expiresInSeconds - this.networkLatency);
        }

        public bool HasExpired()
        {
            return expirationTime <= DateTime.Now ? true : false;
        }
        public TimeSpan TimeToExpiration()
        {
            return (expirationTime - DateTime.Now);
        }
    }
}

