﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ntmcalc_sample_csharp_client
{
    class Program
    {
        static void Main(string[] args)
        {
     
            try
            {
                // Authenticate an get an access token
                var accessToken = Auth.GetAccessToken();
               
                // Putting together POST request parameters to calculate "Rigid truck <7.5 t" with default parameters
                dynamic parameters = new JObject();
                parameters.calculationObject = new JObject();
                parameters.calculationObject.id = "rigid_truck_7_5_t";
                parameters.calculationObject.version = "1";
                
                // Calling the NTMCalc calculation API
                Console.WriteLine("Now calling NTMCalc to calculate Rigid truck <7.5t");
                dynamic response;
                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Headers[HttpRequestHeader.Authorization] = "Bearer " + accessToken;
                    response =
                        JObject.Parse(client.UploadString("https://api.transportmeasures.org/v1/transportactivities",
                            "POST", JsonConvert.SerializeObject(parameters)));
                }
                Console.WriteLine("The total CO2 quantity is " + response.resultTable.totals[(int)response.resultTable.index.co2_total].value + " " + response.resultTable.columnHeaders[(int)response.resultTable.index.co2_total].unit);
                try
                {
                    Console.WriteLine(Auth.EndSession());
                }
                catch (Exception)
                {
                    Console.WriteLine("Logout failed.");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Login failed, no accesstoken was issued");
            }
            Console.WriteLine("Hit any key to end the session");
            Console.ReadKey();
        }
    }
}
